# Json-API

Пакет для простого создания API с учетом версионности на основе simple:json-routes 

## Класс Api

Для работы необходимо переопределить метод `checkAuth`, в котором проверяется доступ к `api` и возвращается ид пользователя.

В конструктор класса передается параметр версии, он должен быть уникальным. По нему осуществляется доступ к разным версиям `api`.

Путь работы `api`: `/api/%version%/%endpoint_name%` 

### Обработка запросов

Для методов используется `функция обработки запроса` которая должна вернуть **объект** из которого будет сформирован ответ клиенту.

Логика работы функции одинакова для всех запросов, различаются только входные параметры.

### Метод get

Создает обработку для метода `GET`. Имеет два параметра: путь и обработка запроса.

- Путь {String} - должен быть уникальным в пределах версии, по нему будут приходить запросы клиента, с учетом версии;
- Обработка запроса {Function} - функция в которую передаются 3 параметра:
  - `_idUser` {String} - ид пользователя, определенный с помощью метода `checkAuth`
  - `query` {Object} - get-параметры запроса
  - `params` {Object} - route-параметры запроса
  

```javascript
import Api, { ApiError } from '/modules/Api/server';
import Api_User from '/modules/Api/server/0.1/api-user';

const api = new Api('0.1');

api.get('/user/list', ({ _idUser, query: { start, end } }) => {
  if (!Api_User.checkAdmin(_idUser)) {
    throw new ApiError({ code: 403, error: 'Доступ запрещен' });
  }
  return { users: Api_User.getByPeriod(start, end) };
});

```

### Метод post

Создает обработку для метода `POST`. Имеет два параметра: путь и обработка запроса.

- Путь {String} - должен быть уникальным в пределах версии, по нему будут приходить запросы клиента, с учетом версии;
- Обработка запроса {Function} - функция в которую передаются 4 параметра:
  - `_idUser` {String} - ид пользователя, определенный с помощью метода `checkAuth`
  - `query` {Object} - get-параметры запроса
  - `params` {Object} - route-параметры запроса
  - `data` {Object} - Объект сформированный из JSON в `body`

```javascript
import Api, { ApiError } from '/modules/Api/server';
import Api_User from '/modules/Api/server/0.1/api-user';
import Api_Order from '/modules/Api/server/0.1/api-order';

const api = new Api('0.1');

api.post('/order/edit/:_id', ({ _idUser, params: { _id }, data: { status } }) => {
  if (!Api_User.checkAdmin(_idUser)) {
    throw new ApiError({ code: 403, error: 'Доступ запрещен' });
  }
  Api_Order.changeStatus(_id, status);
  return { code: 202, data: { message: 'Статус изменен' } };
});

```

## Класс ApiError

Класс для обработки ошибок `API`. Должен использоваться для обработки **ВСЕХ** ошибок. 

Инстансы создаются объектом с 2 полями:
- `code` {Number} - код состояния http
- `error` {String} - текст ошибки

## Класс ApiResult

Класс для формирования ответа клиенту. Результат возвращаемый `функциями обработки запроса` используется для инстантенации этого класса.

По умолчанию код ответа `200`, если необходимо переопределить код ответа, то нужно вернуть вложенный объект 
`{ code, data }`, где:
- `code` {Number} - код состояния http
- `data` {Object} - результат который нужно вернуть клиенту
