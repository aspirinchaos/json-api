// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from 'meteor/tinytest';

// Import and rename a variable exported by json-api.js.
import { name as packageName } from 'meteor/json-api';

// Write your tests here!
// Here is an example.
Tinytest.add('json-api - example', function (test) {
  test.equal(packageName, 'json-api');
});
