import { Meteor } from 'meteor/meteor';
import { JsonRoutes } from 'meteor/simple:json-routes';

const versions = [];

/**
 * Класс для обработки ошибок API
 * Код ошибки должен соответствовать кодам http
 * @see https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%BA%D0%BE%D0%B4%D0%BE%D0%B2_%D1%81%D0%BE%D1%81%D1%82%D0%BE%D1%8F%D0%BD%D0%B8%D1%8F_HTTP
 */
class ApiError {
  /**
   * Формируем ошибку которой ответим
   * @param {Number} code - код ошибки
   * @param {String} error - текст ошибки
   */
  constructor({ code = 500, error }) {
    this.error = error;
    this.code = code;
  }

  /**
   * Отправка ответа на запрос
   * @param {http.ServerResponse} res - node-объект для ответа
   */
  send(res) {
    const { code, error } = this;
    JsonRoutes.sendResult(res, { code, data: { error } });
  }
}

/**
 * Класс для обработки ответа API
 */
class ApiResult {
  /**
   * Формируем ответ
   * @param {Object} result - результат который нужно отдаль клиенту
   * @param {?Number} result.code - когда нужно передать особый код
   * @param {?Object} result.data - вкладываем результат в объект
   */
  constructor(result) {
    if (result.data && result.code) {
      this.data = result.data;
      this.code = result.code;
    } else {
      this.data = result;
      this.code = 200;
    }
  }

  /**
   * Отправка ответа на запрос
   * @param {http.ServerResponse} res - node-объект для ответа
   */
  send(res) {
    const { data, code } = this;
    JsonRoutes.sendResult(res, { code, data });
  }
}

/**
 * Обработка ошибок запроса
 */
JsonRoutes.ErrorMiddleware.use((err, req, res, next) => {
  if (err.body) {
    const contentType = req.headers && req.headers['content-type'];
    const isJson = contentType && contentType.includes('json');
    if (!isJson) {
      (new ApiError({ code: 400, error: 'Запросы принимаются только в формате JSON' }).send(res));
    }
    try {
      JSON.parse(err.body);
    } catch (e) {
      (new ApiError({ code: 400, error: 'Некорректный JSON в запросе' }).send(res));
    }
  }
});

/**
 * Класс для создания API работающего через JSON
 * Использует JsonRoutes для формирования ответов
 */
class Api {
  /**
   * Версия апи
   * @type {string}
   * @private
   */
  _v = '';

  /**
   * Передаем версию API для формирования и разделения роутов
   * @param {String} v - версия апи
   */
  constructor(v) {
    if (!v) {
      throw new Meteor.Error('bad-version-api', 'Задайте версию для API!');
    }
    if (versions.includes(v)) {
      throw new Meteor.Error('bad-version-api', `Версия API ${v} уже определена!`);
    }
    versions.push(v);
    this._v = v;
  }

  /**
   * Базовая проверка авторизации и получение ид пользователя
   * @param {Object} query - параметры запроса
   * @return {String} - ид пользователя
   */
  checkAuth(query) {
    console.error('Нужно определить функцию проверки авторизации');
  }

  /**
   * Убедимся, что путь начинается правильно ☯️
   * @param {String} path - путь
   * @return {String} правильный путь
   * @private
   */
  static _surePath(path) {
    if (path[0] !== '/') {
      return `/${path}`;
    }
    return path;
  }

  /**
   * Обработка get запроса
   * @param {String} path - путь запроса
   * @param {Function} cb - обработка ответа
   */
  get(path, cb) {
    path = Api._surePath(path);
    JsonRoutes.add('get', `/api/${this._v}${path}`, (req, res, next) => {
      try {
        const { query, params } = req;
        const _idUser = this.checkAuth(query);
        Promise.resolve(cb({ _idUser, params, query })).then((result) => {
          (new ApiResult(result).send(res));
        }).catch((e) => {
          if (!e.send) {
            console.log(e);
            // нечего холопам знать что у барина ошибки 🤴
            (new ApiError({ code: 404, error: 'Not Found' })).send(res);
            return;
          }
          e.send(res);
        });
      } catch (e) {
        if (!e.send) {
          console.log(e);
          // нечего холопам знать что у барина ошибки 🤴
          (new ApiError({ code: 404, error: 'Not Found' })).send(res);
          return;
        }
        e.send(res);
      }
    });
  }

  /**
   * Обработка post запроса
   * @param {String} path - путь запроса
   * @param {Function} cb - обработка ответа
   */
  post(path, cb) {
    path = Api._surePath(path);
    JsonRoutes.add('post', `/api/${this._v}${path}`, (req, res, next) => {
      try {
        const { query, params } = req;
        const _idUser = this.checkAuth(query);
        const data = req.body;
        if (!data) {
          throw new ApiError({ code: 400, error: 'Empty request!' });
        }
        Promise.resolve(cb({
          _idUser, params, query, data,
        })).then((result) => {
          (new ApiResult(result).send(res));
        }).catch((e) => {
          if (!e.send) {
            console.log(e);
            // нечего холопам знать что у барина ошибки 🤴
            (new ApiError({ code: 404, error: 'Not Found' })).send(res);
            return;
          }
          e.send(res);
        });
      } catch (e) {
        e.send(res);
      }
    });
  }
}

export { Api, ApiError, ApiResult };
