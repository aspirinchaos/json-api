Package.describe({
  name: 'json-api',
  version: '0.1.1',
  summary: 'Wrapper for simple:json-routes to use with versions',
  git: 'https://bitbucket.org/aspirinchaos/json-api.git',
  documentation: 'README.md',
});

Package.onUse((api) => {
  api.versionsFrom('1.8');
  api.use([
    'ecmascript',
    'simple:json-routes@2.1.0',
  ]);
  api.mainModule('json-api.js', 'server');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('json-api');
  api.mainModule('json-api-tests.js');
});
